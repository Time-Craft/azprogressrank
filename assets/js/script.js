$( document ).ready(function() {
    var minetip = document.getElementById("minetip-tooltip");

    $(".minetext").mouseover(function(event) {
      minetip.innerHTML = event.target.dataset.mctitle;
      minetip.style.display = "block";
    });
    
    $(".minetext").mouseout(function() {
      minetip.style.display = "none";
    });
    
    $(".minetext").mousemove(function (event) {
      pos (minetip, 0, 0, event);
    });

    function getCoords(elem) {
        let box = elem.getBoundingClientRect();
      
        return {
          top: box.top + window.scrollY,
          right: box.right + window.scrollX,
          bottom: box.bottom + window.scrollY,
          left: box.left + window.scrollX
        };
      }
    
    var pos = function (o, x, y, event) {
        var posX = 0, posY = 0;
        //var height = $("header").height()+$("#progress-title").height();
        coords = getCoords(event.target)
        posX = coords.right;
        posY = coords.top;
        console.log(posX +' '+posY);
        o.style.position = "absolute";
        o.style.top = (posY + y) + "px";
        o.style.left = (posX + x) + "px";
    }
    
    minetip.style.display = "none";
})