@extends('layouts.app')

@section('title', 'Progression sur le serveur')

@section('content')

    <div class="page-progress">
        <div id="progress-title">
            <h1>Objectifs en cours</h1>
            @if(!$logged)
            <div class="alert alert-info" role="alert">Tu dois être connecté sur le serveur pour obtenir ta progression précise</div>
            @endif
        </div>
        <div id="minetip-tooltip"><span class="minetip-title" id="minetip-text">Minecraft Tip</span></div>
        <div class="card shadow mb-4">
        
            <div class="card-body">
                @if(!is_null($rank))
                    <div style="text-align: center;">
                        <h2>Ton grade actuel : {!! $rank !!}</h2>
                        <p>{{$description}}</p>
                    </div>
                @endif
                @php 
                $i = -1;
                @endphp
                @forelse ($progress as $path => $tasks)
                    
                    <h2>Objectifs pour devenir {!! $path !!}</h2>
                    @foreach($tasks as $info)
                        @php
                        $i += 1;
                        $mod = ($i % 6)
                        @endphp
                        @if($i == 0)
                            <div class="row">
                        @elseif($mod == 0)
                            </div>
                            <div class="row">
                        @endif
                        <div class="col-md-2">
                            <div class="card mb-2">
                                    <div class="card-body">
                                        <p>{!! $info['description'] !!}</p>
                                        <h5>{{ $info['progress'] }}</h5>
                                    </div>
                            </div>
                        </div>
                    @endforeach
                @empty
                    @if (is_null($rank))
                        <p>Pas de progression en cours</p>
                    @else
                        <p>Tu es actuellement {{ $rank }}, Pour continuer votre progression et atteindre le prochain rang pensez à faire /objectif 1 à 2 fois</p>
                    @endif
                @endforelse
                @if ($i > -1)
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{ plugin_asset('progress', 'js/script.js')  }}"></script>
@endpush
@push('styles')
    <link rel="stylesheet" href="{{ plugin_asset('progress', 'css/icons-minecraft-0.5.css')  }}">
    <link rel="stylesheet" href="{{ plugin_asset('progress', 'css/style.css')  }}">
@endpush
