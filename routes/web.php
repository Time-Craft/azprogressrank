<?php

use Azuriom\Plugin\Progress\Controllers\ProgressController;
use Illuminate\Support\Facades\Route;

Route::get('/', ['Azuriom\Plugin\Progress\Controllers\ProgressController','index'])->name('index');