<?php

namespace Azuriom\Plugin\Progress\Controllers;

use Azuriom\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use xPaw\SourceQuery\SourceQuery;
use Azuriom\Models\Server;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProgressController extends Controller
{
    private const RANKS = [
        "legendaire" => ["<span class='legende'>Légendaire</span>","Nom de Zeus, tu es le maître du jeu"],
        "legende" => ["<span class='maitre'>Légende</span>","Le chemin est long pour devenir Légendaire !"],
        "voyageur" => ["Voyageur","Tu dois lire les règles avec la commande /rules pour accéder au prochain grade<br>Lis attentivement"],
        "resident" => ["<span class='resident'>Résident</span>","Il est grand temps de vivre ici :)"],
        "citoyen" => ["<span class='resident'>Citoyen</span>","Tu as appris les délires du serveur"],
        "patriote" => ["<span class='resident'>Patriote</span>","Te voilà parmi la communauté !"],
        "pickpocket" => ["<span class='voleur'>PickPocket</span>","Luke! Viens avec moi ! _ Oui père. Fin du film"],
        "arnaqueur" => ["<span class='voleur'>Arnaqueur</span>","Tu vends des cartes bleues erronées sur le dark net, malin"],
        "cambrioleur" => ["<span class='voleur'>Cambrioleur</span>","Ta dernière casse ? La banque centrale du pays. Tu porte des lunettes et un masque de Dali ?"],
        "gangster" => ["<span class='voleur'>Gangster</span>","The Big One! Tu l'as fait! Mais tu ne t'appelle pas Trevor"],
        "parrain" => ["<span class='voleur'>Parrain</span>","Je suis un homme d'affaire, et l'sang ça coute trop cher..."],
        "expert-iii" => ["<span class='expert'>Expert III</span>","Avec ce niveau d'expertise, j'espère que tu demande une augmentation"],
        "expert-iv" => ["<span class='expert'>Expert IV</span>","Bientôt on pourra t'appeler MacGyver"],
        "expert-ii" => ["<span class='expert'>Expert II</span>","Un niveau d'expertise tel quel, incroyable"],
        "expert-i" => ["<span class='expert'>Expert I</span>","Une petit pas pour l'homme, un grand pas pour l'humanité"],
        "expert-v" => ["<span class='expert'>Expert V</span>","Si j'aurais su, j'aurais pas venu."],
        "maitre-iii" => ["<span class='maitre'>Maitre III</span>","Un grand pouvoir implique de grandes responsabilités"],
        "maitre-iv" => ["<span class='maitre'>Maitre IV</span>","Vous ne passerez pas !"],
        "maitre-v" => ["<span class='maitre'>Maitre V</span>","Vers l'infini et au-delà !"],
        "maitre-ii" => ["<span class='maitre'>Maitre II</span>","Arrivé jusqu'ici c'est... impossible!"],
        "maitre-i" => ["<span class='maitre'>Maitre I</span>","Tu est désormais un maître Jedi"],
        "soldat" => ["<span class='militaire'>Soldat</span>","Nous venons en paix."],
        "caporal" => ["<span class='militaire'>Caporal</span>","Force et honneur"],
        "sergent" => ["<span class='militaire'>Sergent</span>","J'ai rencontré trois gabardines qui attendaient un train. Dans les gabardines se trouvaient trois hommes. Dans ces hommes se trouvent trois balles."],
        "lieutenant" => ["<span class='militaire'>Lieutenant</span>","Pour survivre à la guerre, il faut devenir la guerre."],
        "commandant" => ["<span class='militaire'>Commandant</span>","Ils peuvent prendre nos vies, mais jamais… notre liberté"],
        "cueilleur" => ["<span class='agriculteur'>Cueilleur</span>","Je finis toujours le travail pour lequel on me paie"],
        "paysan" => ["<span class='agriculteur'>Paysan</span>","La vie, c'est comme une boîte de chocolats"],
        "fermier" => ["<span class='agriculteur'>Fermier</span>","C'est un arbre ! Un arbre qui chante !"],
        "agriculteur" => ["<span class='agriculteur'>Agriculteur</span>","On peut tromper mille fois mille personnes, non, on peut tromper une fois mille personnes, mais on ne peut pas tromper mille fois mille personnes. Attends..."],
        "exploitant" => ["<span class='agriculteur'>Exploitant</span>","Vous savez, moi je ne crois pas qu'il y ait de bonne ou de mauvaise situation. Moi, si je devais résumer ma vie aujourd'hui avec vous, je dirais que c'est d'abord des rencontres"],
        "orpailleur" => ["<span class='mineur'>Orpailleur</span>","Une mine !"],
        "mineur" => ["<span class='mineur'>Mineur</span>","Tu vois, le monde se divise en deux catégories: ceux qui ont un pistolet chargé et ceux qui creusent. Toi tu creuses."],
        "forgeron" => ["<span class='mineur'>Forgeron</span>","Personne ne lance un nain !"],
        "orfevre" => ["<span class='mineur'>Orfèvre</span>","Nous sommes les nains sous la montagne! on dort le jour, on boit la nuit!"],
        "joaillier" => ["<span class='mineur'>Joaillier</span>","Mon précieuux!"],
        "metier" => ["<span class='resident'>Ton premier Job</span>","Laver la voiture du patron ?"]
    ];
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Show the home plugin page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $servers = Server::all(['id', 'name', 'address', 'port', 'data']);
        $server = $servers[0];
        $user = Auth::user(['id','game_id']);
        return $this->rcon($server,$user);
    }

    private function change_key( $array, $old_key, $new_key ) {

        if( ! array_key_exists( $old_key, $array ) )
            return $array;
    
        $keys = array_keys( $array );
        $keys[ array_search( $old_key, $keys ) ] = $new_key;
    
        return array_combine( $keys, $array );
    }

    private function rcon($server, $user)
    {
        $rconPassword = decrypt($server->data['rcon-password'], false);
        $port = $server->data['rcon-port'] ?? $server->port;

        $sq = new SourceQuery();

        try {
            $sq->Connect($server->address, $port,1);
            $sq->SetRconPassword($rconPassword);
        } catch (\Exception $e)
        {
            dump($e);
            return $this->error("rcon impossible");
        }
        $ranks = DB::select("select TRIM(Leading 'group.' FROM permission) as 'group' from LuckPerms.luckperms_user_permissions where permission like 'group.%' and replace(uuid,'-','') = '$user->game_id'");
        $rank = NULL;
        $desc = NULL;
        if ( is_array($ranks) ){
            foreach ($ranks as $lrank){
                if (str_ends_with($lrank->group,'default') || str_contains($lrank->group,'vip'))continue;
                foreach (self::RANKS as $key =>$ldesc){
                    if (str_ends_with($lrank->group,$key)){
                        $rank = $ldesc[0];
                        $desc = $ldesc[1];
                        break;
                    }
                }
                if (!is_null($rank)) break;
            }
        }
        $page = 0;
        $finalres = array();
        $logged = False;
        do{
            $result = $sq->Rcon("aztrack ".$user->game_id." ".$page);

            if (!str_contains($result,'The player should be online to display progress.'))$logged = True;
            $result = preg_replace('/§[0-9a-frl]/','',$result);
            
            $jres = json_decode($result,true);
            if ( !is_array($jres) || !array_key_exists('pages',$jres)){
                return view('progress::index',[
                    'progress'=> $finalres,
                    'logged' => $logged,
                    'rank' => $rank,
                    'description' => $desc
                ]);
            }
            $count = 0;
            foreach (self::RANKS as $keydesc => $ldesc){
                foreach ($jres as $jrankkey => $jrank){
                    str_replace($keydesc,$ldesc[0],$jrankkey,$count);
                    if ($count > 0) {
                        break;
                    }
                }
                if ($count > 0) break;
            }
            $jres =$this->change_key($jres,$jrankkey,$ldesc[0]);
            $remaining = $jres['pages'] - $page;
            unset($jres['pages']);
            
            
            $finalres = array_merge_recursive($finalres,$jres);
            if ($result[0] === "/") return view('progress::index',[
                'progress'=> []
            ]);
            if ($result === false)
                return $this->error('impossible de récupérer le résultat');
            $page += 1;
        }
        while($remaining > 0);

        #return response($result);
        
        return view('progress::index',[
            'progress'=> $finalres,
            'logged' => $logged,
            'rank' => $rank,
            'description' => $desc
        ]);
    }
    private function error(string $msg) : Response
    {
        return response([
            'error' => $msg
        ], 400);
    }
}
